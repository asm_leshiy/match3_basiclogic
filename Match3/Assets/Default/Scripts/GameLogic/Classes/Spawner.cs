﻿using UnityEngine;

namespace ASM.Project
{
    /* ============================= */
    /* SCRIPT_DESCRIPTION */
    /* ============================= */

    public class Spawner<T> where T : BaseObject
    {
        #region VARIABLES

        T[] Array { get; set; }

        #endregion

        // ============================================== //

        public Spawner(T[] array)
        {
            Array = array;
        }

        // ============================================== //

        public void Spawn(Vector3 atPos)
        {
            if (!Array.ExistAndNotEmpty()) return;

            T subject = GetSpawnSubject();
            if (subject == null) return;
            subject.Show(true, atPos);
        }

        public T GetSubject(Vector3 atPos)
        {
            if (!Array.ExistAndNotEmpty()) return null;

            T subject = GetSpawnSubject();
            if (subject != null)
            {
                subject.Show(true, atPos);
                return subject;
            }
            else return null;
        }

        public void ToDefault()
        {
            foreach (var item in Array)
                item.Show(false);
        }

        T GetSpawnSubject()
        {
            for (int i = 0; i < Array.Length; i++)
            {
                if (Array[i].IsActive) continue;
                return Array[i];
            }
            return null;
        }
    }
}