﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project
{
    using Object = UnityEngine.Object;
    using Main;
    using UI;

    /* ============================= */
    /* SIMPLIFIED LINKS TO MANAGERS  */
    /* ============================= */

    public class BaseManager
    {
        protected static ApplicationManager App => ApplicationManager.Instance;
        protected static GameViewManager GameViewMng => GameViewManager.Instance;
        public SoundManager SoundMng => SoundManager.Instance;
    }

    /* =================================== */
    /* DEFAULT NOT MONOBEHAVIOUR OBJECT    */
    /* =================================== */

    [Serializable]
    public class BaseObject
    {
        #region VARIABLES

        [Space(5)]
        [Header("From inheritance")]
        [SerializeField] protected GameObject obj;

        public Transform Tr => obj.transform;
        public Vector3 Pos => Tr.position;
        public bool IsActive => obj.activeSelf;

        #endregion

        // ============================================== //

        public BaseObject() { }

        public BaseObject(GameObject prefab)
        {
            obj = prefab;
        }

        // ============================================== //

        public virtual void Show()
        {
            bool show = !IsActive;
            obj.SetActive(show);
        }

        public virtual void Show(bool show) => obj.SetActive(show);

        public virtual void Show(bool show, Vector3 atPos)
        {
            if (show) Tr.position = atPos;
            Show(show);
        }

        public virtual void SelfDestroy() => Object.Destroy(obj);
    }

    /* =================================== */
    /* DEFAULT MOVABLE OBJECT              */
    /* =================================== */

    public abstract class MovableObject : BaseObject
    {
        #region VARIABLES

        public bool CanMove { get; set; }
        public Vector3[] Route { get; protected set; }
        protected float Speed { get; set; }

        Transform transform;

        #endregion

        // ============================================== //

        protected abstract float RealSpeed();

        public abstract void Movement();

        /// <summary> simple movement from current position with RealSpeed() </summary>
        protected virtual void MoveTo(Vector3 toPos)
        {
            if (!CanMove) return;
            if (transform == null) transform = Tr;
            transform.position = Vector3.MoveTowards(Pos, toPos, Time.deltaTime * RealSpeed());
        }

        public virtual void Stop(bool show = true)
        {
            CanMove = false;
            Show(show);
        }

        public void Freeze(bool can_move)
        {
            CanMove = can_move;
        }
        public void SetRoute(Vector3[] route) => Route = route;
    }

    /* =================================== */
    /* DEFAULT UI PANELS PARENT            */
    /* =================================== */

    [Serializable]
    public class BaseUI : BaseObject
    {
        #region VARIABLES

        [SerializeField] protected Button base_btn;
        public Action BaseAct;

        #endregion

        protected SoundManager SoundMng => SoundMng;

        // ============================================== //

        public virtual void Init()
        {
            base_btn.ClickAction(BaseAct_Default);
        }

        // ============================================== //

        public override void Show()
        {
            if (!IsActive) Tr.SetAsLastSibling();
            base.Show();
        }

        public override void Show(bool show)
        {
            if (show) Tr.SetAsLastSibling();
            obj.SetActive(show);
        }

        public override void Show(bool show, Vector3 atPos)
        {
            if (show) Tr.SetAsLastSibling();
            base.Show(show, atPos);
        }

        protected virtual void BaseAct_Default()
        {
            BaseAct?.Invoke();
            BaseAct = () => { };
            Show();
        }
    }
}