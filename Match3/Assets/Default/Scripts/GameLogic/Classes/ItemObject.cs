﻿using System;
using System.Collections;
using UnityEngine;

namespace ASM.Project
{
    /* ============================= */
    /* DEFAULT LEVEL FIELD ITEM      */
    /* ============================= */

    public class ItemObject : MovableObject
    {
        #region VARIABLES

        public ItemType Type { get; }
        public int Score { get; }
        public bool IsMoving { get; private set; }

        int index;
        Action onEndMovement;

        #endregion

        // ============================================== //

        public ItemObject(ItemType type, int score, GameObject obj)
        {
            this.obj = obj;
            Type = type;
            Score = score;
            Speed = 1;
            index = 0;
            CanMove = false;
            IsMoving = false;
            onEndMovement = null;
        }

        protected override float RealSpeed() => Speed;

        /// <summary> call from Update </summary>
        public override void Movement()
        {
            if (Route == null || Route.Length == 0 || Speed == 0 || !IsActive) return;

            IsMoving = true;
            if (Pos == Route[index]) index += 1;

            if (index >= Route.Length)
            {
                onEndMovement?.Invoke();
                Stop();
            }
            else MoveTo(Route[index]);
        }

        // ============================================== //

        /// <summary> are objects equal by ItemType </summary>
        public bool Equals(ItemObject other) => Type == other.Type;

        /// <summary> allow linear movement by route </summary>
        public void StartMovement(float speed, Vector3[] route, Action callback = null)
        {
            if (IsMoving) return;

            onEndMovement = callback;
            Speed = speed;
            index = 0;
            SetRoute(route);
            CanMove = true;
        }

        /// <summary> stop object movement </summary>
        public void Stop()
        {
            IsMoving = false;
            Route = null;
            Speed = 0;
            onEndMovement = null;
        }

        /// <summary> on match3 animation </summary>
        public IEnumerator BounceAnim(Action callback)
        {
            var localScale = Tr.localScale;
            var scaleX = AnimationCurve.Linear(0f, localScale.x, 1f, localScale.x);
            var scaleY = AnimationCurve.Linear(0f, localScale.y, 1f, localScale.y);
            scaleX.AddKey(0.5f, localScale.x + .1f);
            scaleY.AddKey(0.5f, localScale.y + .1f);

            var time = 0f;
            while (time < 1)
            {
                time += Time.deltaTime;
                Tr.localScale = new Vector3(scaleX.Evaluate(time), scaleY.Evaluate(time));
                yield return null;
            }

            callback?.Invoke();
        }
    }
}