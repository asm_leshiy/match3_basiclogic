﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ASM.Project.Game
{
    /* =============================== */
    /* FIELD_ITEMS INTERACT CONTROLLER */
    /* =============================== */

    public sealed class InputManager : MonoBehaviour
    {
        #region VARIABLES

        static InputManager Instance { get; set; }

        LevelManager mng;
        bool enabledInput;

        Vector2 fromPos;
        Vector2 toPos;
        bool track;

        #endregion

        // ========================================== [ UNITY BASICS ]

        private void Awake()
        {
            if (Instance == null) Instance = this;
            else Destroy(this);
        }

        private void OnEnable()
        {
            UpdateSystem.OnUpdateHandler += OnUpdate;
            UpdateSystem.InputSystemHandler += EnableInput;
        }

        private void OnDisable()
        {
            UpdateSystem.OnUpdateHandler -= OnUpdate;
            UpdateSystem.InputSystemHandler -= EnableInput;
        }

        private void OnUpdate()
        {
            if (!enabledInput) return;
            
            MouseInput();
        }

        // ========================================== [ MAIN LOGIC ]

        public InputManager Init(LevelManager level_mng)
        {
            mng = level_mng;
            EnableInput(false);
            return this;
        }

        void EnableInput(bool enable)
        {
            enabledInput = enable;
        }

        void MouseInput()
        {
            if (mng == null) return;
            if (EventSystem.current.IsPointerOverGameObject()) return;

            if (Input.GetMouseButtonDown(0)) LMB_Down();
            if (Input.GetMouseButton(0)) LMB_HeldDown();
            if (Input.GetMouseButtonUp(0)) LMB_Up();
        }

        void LMB_Down()
        {
            var mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            fromPos = transform.InverseTransformPoint(mouse_pos).RoundToInt();
            track = mng.AtRange(fromPos);
        }

        void LMB_HeldDown()
        {
            if (!track) return;

            Vector2 mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            toPos = transform.InverseTransformPoint(mouse_pos).RoundToInt();

            var dist = Vector2.Distance(fromPos, toPos);
            if (dist >= .75f && dist <= 1.25f)
            {
                if (mng.CanSwitchItems(fromPos, toPos))
                    track = false;
            }
        }

        void LMB_Up()
        {
            track = false;
        }
    }
}