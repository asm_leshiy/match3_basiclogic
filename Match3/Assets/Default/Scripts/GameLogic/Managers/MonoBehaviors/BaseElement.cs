﻿using UnityEngine;

namespace ASM.Project
{
    using Main;
    using UI;

    /* ============================= */
    /* SIMPLIFIED LINKS TO MANAGERS  */
    /* ============================= */

    public class BaseElement : MonoBehaviour
    {
        protected static ApplicationManager App => ApplicationManager.Instance;
        protected static GameViewManager ViewMng => GameViewManager.Instance;
        protected static SoundManager SoundMng => SoundManager.Instance;
    }
}