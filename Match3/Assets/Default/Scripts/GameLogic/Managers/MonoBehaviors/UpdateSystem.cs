﻿using System;
using UnityEngine;

namespace ASM.Project
{
    /* ============================= */
    /* MAIN GAME UPDATES AND EVENTS  */
    /* ============================= */

    public sealed class UpdateSystem : MonoBehaviour
    {
        #region VARIABLES

        static UpdateSystem Instance { get; set; }

        public static event Action OnUpdateHandler;
        public static event Action<bool> InputSystemHandler;

        #endregion

        // ========================================== [ UNITY BASICS ]

        private void Awake()
        {
            if (Instance == null) Instance = this;
            else Destroy(this);
        }

        private void Update()
        {
            OnUpdateHandler.Invoke();
        }

        // ==========================================

        /// <summary> call on change input_system state </summary>
        public static void EnableInput(bool enable)
        {
            InputSystemHandler.Invoke(enable);
        }
    }
}