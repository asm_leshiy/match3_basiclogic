﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ASM.Project.Main
{
    using Data;
    using Game;

    /* ============================= */
    /* MAIN APPLICATION CONTROLLER   */
    /* ============================= */

    [RequireComponent(typeof(UpdateSystem))]
    public sealed class ApplicationManager : BaseElement
    {
        #region VARIABLES

        public static ApplicationManager Instance { get; private set; }

        [SerializeField] GameConfigData gameData;

        LevelManager gameMng;

        #endregion

        // ========================================== [ UNITY BASICS ]

        #region UNITY BASICS

        private void Awake()
        {
            if (Instance == null) Instance = this;
            else Destroy(gameObject);

            DontDestroyOnLoad(gameObject);

            gameObject.AddComponent<UpdateSystem>();
            ObjectPool_Init();
        }

        #endregion

        // ========================================== [ MAIN LOGIC ]

        void ObjectPool_Init()
        {
            GameObject obj = new GameObject("ObjectPool", typeof(LevelManager));
            obj.transform.SetParent(transform);
            gameMng = obj.GetComponent<LevelManager>();

            List<ItemObject> samples = new List<ItemObject>
            {
                new ItemObject(ItemType.BACKGROUND, 0, gameData.levelField.BgPrefab())
            };
            foreach (var item in gameData.items)
            {
                if (!item.IsSet) continue;
                samples.Add(new ItemObject(item.Type, item.Scores, item.Prefab()));
            }
            gameMng.Init(samples);
        }

        public void CreateLevelField(int width, int height)
        {
            GameConfig config = new GameConfig(width, height, width * height * 30);
            ViewMng.SetConfig(config);
            gameMng.NewLevel_Init(config);
            gameMng.gameObject.AddComponent<InputManager>().Init(gameMng);

            var offset_x = ((config.Width / 2f) - .5f) * -1;
            var offset_y = ((config.Height / 2f) - .5f) * -1;
            gameMng.transform.position = new Vector2(offset_x, offset_y);

            int offset = config.Height > config.Width ? config.Height / config.Width + config.Height % config.Width : 0;
            Camera.main.orthographicSize = config.Width + offset;

            gameMng.StartGame();
        }

        public void GameOver()
        {
            gameMng.OnGameOver();
        }
        public void Restart()
        {
            gameMng.ToDefault();
        }
        public void Quit()
        {
            Application.Quit();
        }
    }
}