﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

namespace ASM.Project.Game
{
    using Random = UnityEngine.Random;
    using Data;
    
    /* ============================= */
    /* MATCH_3 GAME LOGIC MANAGER    */
    /* ============================= */

    public sealed class LevelManager : BaseElement
    {
        #region VARIABLES

        static LevelManager Instance { get; set; }
        readonly Dictionary<ItemType, Spawner<BaseObject>> spawner = new Dictionary<ItemType, Spawner<BaseObject>>();

        bool _enableInput;
        bool EnableInput
        {
            get { return _enableInput; }
            set
            {
                _enableInput = value;
                UpdateSystem.EnableInput(_enableInput);
            }
        }

        GameConfig config;
        public ItemObject[,] items;

        const float SWITCH_SPEED = 2;
        const float FALL_SPEED = 4;

        #endregion

        // ========================================== [ UNITY BASICS ]

        public void Awake()
        {
            if (Instance == null) Instance = this;
            else Destroy(this);
        }

        private void OnEnable() => UpdateSystem.OnUpdateHandler += OnUpdate;
        private void OnDisable() => UpdateSystem.OnUpdateHandler -= OnUpdate;

        public void OnUpdate()
        {
            if (items == null) return;

            for (int x = 0; x < config.Width; x++)
            {
                for (int y = 0; y < config.Height; y++)
                {
                    items[x, y]?.Movement();
                }
            }
        }

        // ========================================== [ MAIN LOGIC ]

        public void Init(List<ItemObject> samples)
        {
            if (spawner.ExistAndNotEmpty()) spawner.Clear();

            var count = GameConfig.MaxItemCount;
            foreach (var item in samples)
            {
                var parent = new GameObject(item.Type.ToString()).transform;
                parent.SetParent(transform);
                parent.localPosition = Vector3.zero;

                BaseObject[] array = new BaseObject[count];
                for (int i = 0; i < count; i++)
                {
                    var prefab = Instantiate(item.Tr.gameObject, parent);
                    var obj = new ItemObject(item.Type, item.Score, prefab);
                    obj.Show(false);
                    array[i] = obj;
                }

                if (!spawner.ContainsKey(item.Type))
                    spawner.Add(item.Type, new Spawner<BaseObject>(array));
            }
        }

        public void NewLevel_Init(GameConfig config)
        {
            HideSpawnerObjects();
            this.config = config;
            config.AvailableItems = GetAvailableItemTypes();

            for (int y = 0; y < config.Height; y++)
            {
                for (int x = 0; x < config.Width; x++)
                    spawner[ItemType.BACKGROUND].GetSubject(transform.TransformPoint(new Vector2(x, y)));
            }

            RandomGeneration(config);
        }

        public void StartGame()
        {
            EnableInput = true;
        }

        /// <summary> can we switch items from - to array positions </summary>
        public bool CanSwitchItems(Vector2 from, Vector2 to)
        {
            if (!AtRange(to)) return false;

            bool vertical = from.x == to.x && from.y != to.y;
            bool horizontal = from.x != to.x && from.y == to.y;

            if (vertical || horizontal)
            {
                ItemObject first = items[(int)from.x, (int)from.y];
                ItemObject second = items[(int)to.x, (int)to.y];

                if (first.IsMoving || second.IsMoving) return false;

                SwitchItems(from, to);
                return true;
            }

            return false;
        }

        void SwitchItems(Vector2 from, Vector2 to)
        {
            EnableInput = false;

            Vector2 from_pos = items[(int)from.x, (int)from.y].Tr.parent.TransformPoint(from);
            Vector2 to_pos = items[(int)to.x, (int)to.y].Tr.parent.TransformPoint(to);

            if (items[(int)from.x, (int)from.y].Type == items[(int)to.x, (int)to.y].Type)
            {
                items[(int)from.x, (int)from.y].StartMovement(SWITCH_SPEED, new Vector3[] { to_pos, from_pos });
                items[(int)to.x, (int)to.y].StartMovement(SWITCH_SPEED, new Vector3[] { from_pos, to_pos }, delegate () { EnableInput = true; });
                return;
            }

            Action Switch = () =>
            {
                var temp = items[(int)from.x, (int)from.y];
                items[(int)from.x, (int)from.y] = items[(int)to.x, (int)to.y];
                items[(int)to.x, (int)to.y] = temp;
            };
            Switch.Invoke();

            bool[,] removeItemMap = null;
            if (FindMatches(items, ref removeItemMap))
            {
                items[(int)from.x, (int)from.y].StartMovement(SWITCH_SPEED, new Vector3[] { from_pos });
                items[(int)to.x, (int)to.y].StartMovement(SWITCH_SPEED, new Vector3[] { to_pos }, delegate() 
                {
                    StartCoroutine(ClearFieldProcess(removeItemMap, items));
                });
            }
            else
            {
                Switch.Invoke();
                items[(int)from.x, (int)from.y].StartMovement(SWITCH_SPEED, new Vector3[] { to_pos, from_pos });
                items[(int)to.x, (int)to.y].StartMovement(SWITCH_SPEED, new Vector3[] { from_pos, to_pos }, delegate () { EnableInput = true; });
            }
        }

        bool FindMatches(ItemObject[,] items, ref bool[,] map)
        {
            int width = items.GetLength(0);
            int height = items.GetLength(1);

            map = new bool[width, height];
            bool isTrue = false;

            for (int x = 0; x < width; x++)
            {
                for (int y = 1; y < height - 1; y ++)
                {
                    if (items[x, y] == null || items[x, y + 1] == null || items[x, y - 1] == null) continue;
                    bool same_up = items[x, y + 1].Type == items[x, y].Type;
                    bool same_down = items[x, y - 1].Type == items[x, y].Type;

                    if(same_up && same_down)
                        map[x, y] = map[x, y + 1] = map[x, y - 1] = isTrue = true;
                }
            }

            for (int y = 0; y < height; y++)
            {
                for (int x = 1; x < width - 1; x++)
                {
                    if (items[x, y] == null || items[x + 1, y] == null || items[x - 1, y] == null) continue;
                    bool same_left = items[x + 1, y].Type == items[x, y].Type;
                    bool same_right = items[x - 1, y].Type == items[x, y].Type;

                    if (same_left && same_right)
                        map[x, y] = map[x + 1, y] = map[x - 1, y] = isTrue = true;
                }
            }

            return isTrue;
        }

        bool LevelFielsIsFool(ItemObject[,] items)
        {
            int width = items.GetLength(0);
            int height = items.GetLength(1);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (items[x, y] == null)
                        return false;
                }
            }

            return true;
        }

        bool Match3Possibility(ItemObject[,] items)
        {
            int width = items.GetLength(0);
            int height = items.GetLength(1);

            // vertical check
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (y + 2 == height) break;
                    if (items[x, y].Type != items[x, y + 1].Type) continue;
                    bool match = VerticalMatchPossibility(x, y + 2, items[x, y].Type);
                    if (match) return true;
                }
            }

            // horizontal check
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (x + 2 == width) break;
                    if (items[x, y].Type != items[x + 1, y].Type) continue;
                    bool match = HorizontalMatchPossibility(x + 2, y, items[x, y].Type);
                    if (match) return true;
                }
            }

            return false;
        }

        bool VerticalMatchPossibility(int target_x, int target_y, ItemType current)
        {
            if (target_y + 1 < config.Height)
                if (items[target_x, target_y + 1].Type == current) return true;

            if (target_x + 1 < config.Width)
                if (items[target_x + 1, target_y].Type == current) return true;

            if (target_x - 1 >= 0)
                if (items[target_x - 1, target_y].Type == current) return true;

            return false;
        }

        bool HorizontalMatchPossibility(int target_x, int target_y, ItemType current)
        {
            if (target_x + 1 < config.Width)
                if (items[target_x + 1, target_y].Type == current) return true;

            if (target_y + 1 < config.Height)
                if (items[target_x, target_y + 1].Type == current) return true;

            if (target_y - 1 >= 0)
                if (items[target_x, target_y - 1].Type == current) return true;

            return false;
        }

        ItemObject CreateItem(int x, int y)
        {
            int rnd = Random.Range(0, config.AvailableItems.Count);
            var item_type = config.AvailableItems[rnd];
            return (ItemObject)spawner[item_type].GetSubject(transform.TransformPoint(new Vector3(x, y)));
        }

        IEnumerator ClearFieldProcess(bool[,] map, ItemObject[,] items)
        {
            int width = map.GetLength(0);
            int height = map.GetLength(1);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (map[x, y] == true)
                    {
                        config.Score += items[x, y].Score;
                        int X = x, Y = y;
                        StartCoroutine(items[x, y].BounceAnim(delegate() 
                        {
                            CleanAt(X, Y);
                        }));
                    }
                }
            }
            ViewMng.SetScores(config.Score);

            yield return new WaitWhile(() => LevelFielsIsFool(items));

            StartCoroutine(FillTheFieldProcess(items));
        }

        IEnumerator FillTheFieldProcess(ItemObject[,] items)
        {
            int width = items.GetLength(0);
            int height = items.GetLength(1);
            bool run = true;
            while (run)
            {
                int empty = 0;
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        int next_y = y - 1 >= 0 ? y - 1 : 0;

                        if (items[x, y] == null)
                        {
                            if(y == height - 1)
                            {
                                if (items[x, next_y] == null)
                                    items[x, y] = CreateItem(x, y);
                                else if (items[x, next_y] != null && !items[x, next_y].IsMoving)
                                    items[x, y] = CreateItem(x, y);
                            }

                            empty++;
                            continue;
                        }

                        if (items[x, y].IsMoving) continue;

                        if (items[x, next_y] == null)
                        {
                            items[x, next_y] = items[x, y];
                            items[x, y] = null;
                            Vector2 pos = items[x, next_y].Tr.parent.TransformPoint(new Vector2(x, next_y));
                            items[x, next_y].StartMovement(FALL_SPEED, new Vector3[] { pos });
                        }
                    }
                }
                yield return null;
                run = empty > 0;
            }

            bool[,] removeItemMap = null;
            if (FindMatches(items, ref removeItemMap))
                StartCoroutine(ClearFieldProcess(removeItemMap, items));
            else if (Match3Possibility(items)) EnableInput = true;
            else
            {
                ViewMng.NoMatch3Possibility();
                yield return new WaitForSeconds(1);
                HideItemObjects();
                yield return new WaitForSeconds(.5f);
                RandomGeneration(config);
                EnableInput = true;
            }
        }

        /// <summary> is position at grid bounds </summary>
        public bool AtRange(Vector2 pos) => (pos.x >= 0 && pos.x <= config.Width) && (pos.y >= 0 && pos.y <= config.Height);

        bool GetSameDown(int from_x, int from_y, ItemType sample, ref List<int> chain)
        {
            if (from_y == 0) return false;

            for (int y = from_y - 1; y >= 0; y--)
            {
                if (items[from_x, y] == null) break;
                if (items[from_x, y].Type != sample) break;
                chain.Add(y);
            }

            return chain.Count > 0;
        }

        bool GetSameLeft(int from_x, int from_y, ItemType sample, ref List<int> chain)
        {
            if (from_x == 0) return false;

            for (int x = from_x - 1; x >= 0; x--)
            {
                if (items[x, from_y] == null) break;
                if (items[x, from_y].Type != sample) break;
                chain.Add(x);
            }

            return chain.Count > 0;
        }

        void RandomGeneration(GameConfig config)
        {
            items = new ItemObject[config.Width, config.Height];

            for (int x = 0; x < config.Width; x++)
            {
                for (int y = 0; y < config.Height; y++)
                {
                    config.AvailableItems.Shuffle();
                    var item_type = config.AvailableItems[0];

                    foreach (var item in config.AvailableItems)
                    {
                        var chain_down = new List<int>();
                        if (GetSameDown(x, y, item, ref chain_down) && chain_down.Count >= 2) continue;

                        var chain_left = new List<int>();
                        if (GetSameLeft(x, y, item, ref chain_left) && chain_left.Count >= 2) continue;

                        item_type = item;
                        break;
                    }

                    items[x, y] = (ItemObject)spawner[item_type].GetSubject(transform.TransformPoint(new Vector3(x, y)));
                }
            }

            if (!Match3Possibility(items))
            {
                HideSpawnerObjects();
                RandomGeneration(config);
            }
        }

        IList<ItemType> GetAvailableItemTypes()
        {
            var list = new List<ItemType>();
            foreach (var item in spawner)
            {
                if (item.Key == ItemType.NONE || item.Key == ItemType.BACKGROUND) continue;
                list.Add(item.Key);
            }
            return list;
        }

        void CleanAt(int x, int y)
        {
            items[x, y].Show(false);
            items[x, y] = null;
        }

        public void ToDefault()
        {
            HideSpawnerObjects();
            InputManager target = GetComponent<InputManager>();
            if (target != null) Destroy(target);
        }

        public void OnGameOver()
        {
            StopAllCoroutines();
            for (int x = 0; x < items.GetLength(0); x++)
            {
                for (int y = 0; y < items.GetLength(1); y++)
                {
                    items[x, y]?.Stop();
                }
            }
            items = null;
        }

        void HideSpawnerObjects()
        {
            foreach (var item in spawner)
                spawner[item.Key].ToDefault();
        }
        void HideItemObjects()
        {
            foreach (var item in spawner)
            {
                if (item.Key == ItemType.BACKGROUND) continue;
                spawner[item.Key].ToDefault();
            }
        }
    }
}