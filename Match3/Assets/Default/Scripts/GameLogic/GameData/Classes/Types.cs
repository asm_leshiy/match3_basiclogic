﻿namespace ASM.Project
{
    public enum ItemType
    {
        NONE,
        YELLOW,
        ORANGE,
        RED,
        PURPLE,
        BLUE,
        BACKGROUND
    }

    public enum SoundType
    {
        BUTTON_CLICK,
        UI_SHOW
    }
}