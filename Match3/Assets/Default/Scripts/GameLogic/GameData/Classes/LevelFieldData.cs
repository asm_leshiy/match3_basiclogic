﻿using System;
using UnityEngine;

namespace ASM.Project.Data
{
    [Serializable]
    public class LevelFieldData
    {
        [SerializeField] Color color;
        [SerializeField] SpriteRenderer bg_prefab;

        public GameObject BgPrefab()
        {
            bg_prefab.color = color;
            return bg_prefab.gameObject;
        }
    }
}