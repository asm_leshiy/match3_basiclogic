﻿using System.Collections.Generic;

namespace ASM.Project
{
    /* ============================= */
    /* SCRIPT_DESCRIPTION */
    /* ============================= */

    public class GameConfig
    {
        #region VARIABLES

        public const int MIN_FIELD_SIZE= 3;
        public const int MAX_FIELD_SIZE = 15;

        public static int MaxItemCount => MAX_FIELD_SIZE * MAX_FIELD_SIZE;

        public int Width { get; private set; }
        public int Height { get; private set; }
        public int Score { get; set; }
        public int ScoresToWin { get; }
        public IList<ItemType> AvailableItems { get; set; }

        #endregion

        public int ItemsCount => (Width * Height) / 2;

        // ============================================== //

        public GameConfig(int width, int height, int score_to_win)
        {
            Width = width;
            Height = height;
            Score = 0;
            AvailableItems = null;
            ScoresToWin = score_to_win;
        }

        // ============================================== //
    }
}