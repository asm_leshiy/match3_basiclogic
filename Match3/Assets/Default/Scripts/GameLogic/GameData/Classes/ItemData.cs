﻿using System;
using UnityEngine;

namespace ASM.Project.Data
{
    /* ============================= */
    /* SCRIPT_DESCRIPTION */
    /* ============================= */

    [Serializable]
    public class ItemData
    {
        #region VARIABLES

        [SerializeField] ItemType type;
        [SerializeField] Color color;
        [SerializeField] int scores;
        [SerializeField] SpriteRenderer prefab;

        #endregion

        public ItemType Type => type;
        public int Scores => scores;
        public bool IsSet => Type != ItemType.NONE && prefab != null;

        // ============================================== //

        public GameObject Prefab()
        {
            prefab.color = color;
            return prefab.gameObject;
        }
    }
}