﻿using UnityEngine;

namespace ASM.Project.Data
{
    /* ============================= */
    /* SCRIPT_DESCRIPTION */
    /* ============================= */

    [CreateAssetMenu(fileName = "ItemConfigData", menuName = "ASM.Project/ItemConfigData", order = 1)]
    public class GameConfigData : ScriptableObject
    {
        #region VARIABLES

        public LevelFieldData levelField;
        public ItemData[] items;

        #endregion

        // ============================================== //
        // ============================================== //

        public bool ItemExists(ItemType type)
        {
            foreach (var item in items)
            {
                if (item.Type != type) continue;
                return item.IsSet;
            }
            return false;
        }
    }
}