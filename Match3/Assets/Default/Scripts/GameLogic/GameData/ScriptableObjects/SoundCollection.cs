﻿using UnityEngine;

namespace ASM.Project.Data
{
    [CreateAssetMenu(fileName = "SoundCollection", menuName = "ASM.Project/SoundCollection", order = 1)]
    public class SoundCollection : ScriptableObject
    {
        public AudioClip[] Collection;
    }
}