﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ASM.Project
{
    using Random = UnityEngine.Random;
    using Main;

    /* ============================= */
    /* USER INTERFACE EXTENSION      */
    /* ============================= */

    public static class UiUtil
    {
        public static Button ClickAction(this Button btn, Action action)
        {
            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(() =>
            {
                SoundManager.Instance.Play(SoundType.BUTTON_CLICK);
                action?.Invoke();
            });
            return btn;
        }
    }

    /* ============================= */
    /* OTHER EXTENSION               */
    /* ============================= */

    public static class Util
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            for (int i = list.Count - 1; i > 0; i--)
            {
                int rnd = Random.Range(0, i);
                T temp = list[i];
                list[i] = list[rnd];
                list[rnd] = temp;
            }
        }

        public static Vector3 RoundToInt(this Vector3 pos)
        {
            return new Vector3(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y), Mathf.RoundToInt(pos.z));
        }

        public static bool ExistAndNotEmpty(this ICollection collection)
        {
            return collection != null && collection.Count != 0;
        }

    }
}