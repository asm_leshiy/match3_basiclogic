﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    /* ====================================== */
    /* CREATES LEVEL USING USER INPUT VALUES  */
    /* ====================================== */

    [Serializable]
    public class CreateLevelPanel : BaseAnimPanel
    {
        #region VARIABLES

        [SerializeField] InputField width_input;
        [SerializeField] InputField height_input;

        GameViewManager mng;

        int minLevelSize;
        int maxLevelSize;
        int width;
        int height;

        Regex regex = new Regex(@"^\d+$");

        #endregion

        // ============================================== //

        public void Init(GameViewManager mng, int min_level_size, int max_level_size)
        {
            base.Init();

            this.mng = mng;
            mng.AddLayout(this);

            minLevelSize = min_level_size;
            maxLevelSize = max_level_size;

            width_input.text = minLevelSize.ToString();
            width_input.onValueChanged.AddListener(OnChangeWidthValue);

            height_input.text = minLevelSize.ToString();
            height_input.onValueChanged.AddListener(OnChangeHeightValue);

            base_btn.ClickAction(delegate ()
            {
                if (ValidateInput(width_input.text, ref width) && ValidateInput(height_input.text, ref height))
                {
                    mng.OnCreateLevelBtnClick(width, height);
                    Show();
                }
                else Msg.text = "error : not valid data format";
            });
        }

        public override void Show()
        {
            base.Show();
            Msg.text = "min level size = 3x3\nmax level size = 15x15";
        }

        // ============================================== //

        void OnChangeWidthValue(string str)
        {
            if (str.Length > 2)
                str = str.Remove(2);
            width_input.text = str;
        }

        void OnChangeHeightValue(string str)
        {
            if (str.Length > 2)
                str = str.Remove(2);
            height_input.text = str;
        }

        bool ValidateInput(string str, ref int result)
        {
            if (regex.IsMatch(str))
            {
                result = int.Parse(str);
                Mathf.Clamp(result, minLevelSize, maxLevelSize);
                return true;
            }

            return false;
        }
    }
}