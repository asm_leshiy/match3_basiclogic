﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    [Serializable]
    public class MainMenuPanel : BaseUI
    {
        #region VARIABLES

        [SerializeField] Animator Anim;
        [SerializeField] Button play_btn;

        GameViewManager mng;

        #endregion

        // ============================================== //

        public void Init(GameViewManager mng)
        {
            base.Init();

            this.mng = mng;
            mng.AddLayout(this);

            play_btn.ClickAction(delegate() 
            {
                mng.OnPlayBtnClick();
                Show();
            });

            BaseAct = mng.OnQuitBtnClick;
        }

        public override void Show()
        {
            if (!IsActive) Tr.SetAsLastSibling();

            Anim.SetTrigger("show");
        }

        // ============================================== //

    }
}