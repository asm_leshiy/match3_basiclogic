﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    [Serializable]
    public class StatsPanel : BaseUI
    {
        #region VARIABLES

        [SerializeField] Text score_txt;
        [SerializeField] Text timer_txt;
        [SerializeField] Text scoreToWin_txt;

        GameViewManager mng;

        #endregion

        // ============================================== //

        public void Init(GameViewManager mng)
        {
            base.Init();

            this.mng = mng;
            base_btn.ClickAction(mng.OnPauseBtnClick);

            ToDefault();

            Show(false);
        }

        public override void Show(bool show)
        {
            if (show) Tr.SetAsFirstSibling();
            obj.SetActive(show);
        }

        // ============================================== //

        public void SetScore(int score) => score_txt.text = $"score : {score}";
        public void SetTimer(int timer) => timer_txt.text = $"timer:\n{timer}";
        public void SetWinCondition(int score) => scoreToWin_txt.text = $"required : {score}";
        public void ToDefault()
        {
            score_txt.text = "0";
            timer_txt.text = "0";
            scoreToWin_txt.text = "0";
        }
    }
}