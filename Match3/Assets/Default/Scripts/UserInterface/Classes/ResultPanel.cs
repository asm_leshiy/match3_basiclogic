﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    [Serializable]
    public class ResultPanel : ConfirmAnimPanel
    {
        #region VARIABLES

        GameViewManager mng;

        #endregion

        // ============================================== //

        public void Init(GameViewManager mng)
        {
            base.Init();

            this.mng = mng;
            mng.AddLayout(this);

            BaseAct = mng.OnQuitBtnClick;

            confirm_btn.ClickAction(delegate() 
            {
                mng.OnRestartBtnClick();
                Show();
            });
        }

        // ============================================== //
    }
}