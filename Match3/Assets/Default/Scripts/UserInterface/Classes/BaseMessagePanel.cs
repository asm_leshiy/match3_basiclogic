﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    /* =================================== */
    /* "ONE BUTTON" MESSAGE PANEL          */
    /* =================================== */

    [Serializable]
    public class BaseMessagePanel : BaseAnimPanel
    {
        #region VARIABLES

        [SerializeField] protected Text Stats;
        [SerializeField] protected Text Value;

        const char SEPARATOR = '#';

        #endregion

        // ============================================== //

        public override void Init()
        {
            base.Init();
            Stats.text = string.Empty;
            Value.text = string.Empty;
        }

        // ============================================== //

        public static string Combine(string stats, string values) => stats + SEPARATOR + values;

        public void TwoColumnContent(string title, string combine_str)
        {
            Title.text = title;
            string[] array = combine_str.Split(SEPARATOR);
            Stats.text = array[0];
            Value.text = array[1];
            Msg.text = string.Empty;
        }
    }
}