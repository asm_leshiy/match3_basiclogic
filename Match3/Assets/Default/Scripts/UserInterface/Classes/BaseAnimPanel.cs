﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    /* =================================== */
    /* "ONE BUTTON" DEFAULT ANIMATED PANEL */
    /* =================================== */

    [Serializable]
    public class BaseAnimPanel : BaseUI
    {
        #region VARIABLES

        public new Transform Tr { get; protected set; }

        [SerializeField] protected Animator Anim;
        [SerializeField] protected Text Title;
        [SerializeField] protected Text Msg;

        #endregion

        // ============================================== //

        public override void Init()
        {
            Tr = Anim.transform;
            Msg.text = string.Empty;

            base.Init();
        }

        public override void Show()
        {
            if (!IsActive) Tr.SetAsLastSibling();

            Anim.SetTrigger("show");
        }

        // ============================================== //

        public virtual void SetPanel(string title, string msg, Action onClick = null)
        {
            Title.text = title;
            Msg.text = msg;
            BaseAct = onClick;
        }
    }
}