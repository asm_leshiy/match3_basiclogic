﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    [Serializable]
    public class PauseMenuPanel : BaseUI
    {
        #region VARIABLES

        [SerializeField] Animator Anim;
        [SerializeField] Button resume_btn;
        [SerializeField] Button restart_btn;

        GameViewManager mng;

        #endregion

        // ============================================== //

        public void Init(GameViewManager mng)
        {
            base.Init();

            this.mng = mng;
            mng.AddLayout(this);

            resume_btn.ClickAction(Show);

            restart_btn.ClickAction(delegate ()
            {
                mng.OnRestartBtnClick();
                Show();
            });

            BaseAct = mng.OnQuitBtnClick;
        }

        public override void Show()
        {
            if (!IsActive) Tr.SetAsLastSibling();

            Anim.SetTrigger("show");
        }

        // ============================================== //
    }
}