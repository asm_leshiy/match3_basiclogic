﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    /* ============================= */
    /* USER INTERFACE MANAGER        */
    /* ============================= */

    public class GameViewManager : BaseElement
    {
        #region VARIABLES

        public static GameViewManager Instance { get; private set; }
        public bool GameIsRunning { get; private set; }

        [SerializeField] MainMenuPanel mainMenu;
        [SerializeField] CreateLevelPanel createLevelPanel;
        [SerializeField] PauseMenuPanel pauseMenuPanel;
        [SerializeField] StatsPanel statsPanel;
        [SerializeField] ResultPanel resultPanel;
        [SerializeField] MessagePanel messagePanel;

        GameConfig config;
        IEnumerator timerProcess;

        List<BaseUI> Layouts = new List<BaseUI>();

        #endregion

        // ========================================== [ UNITY BASICS ]

        #region UNITY BASICS

        private void Awake()
        {
            if (Instance == null) Instance = this;
            else Destroy(gameObject);

            mainMenu.Init(this);
            createLevelPanel.Init(this, GameConfig.MIN_FIELD_SIZE, GameConfig.MAX_FIELD_SIZE);
            pauseMenuPanel.Init(this);
            statsPanel.Init(this);
            resultPanel.Init(this);
            messagePanel.Init();
            AddLayout(messagePanel);
        }

        private void Start()
        {
            ShowLayout<MainMenuPanel>();
        }

        #endregion

        // ========================================== [ MAIN LOGIC ]

        public void SetConfig(GameConfig config)
        {
            this.config = config;
            statsPanel.SetWinCondition(config.ScoresToWin);
        }

        public void ShowLayout<T>() where T : BaseUI
        {
            Layouts.ForEach(panel =>
            {
                if (panel.GetType() == typeof(T))
                {
                    panel.Show();
                }
            });
        }

        public void AddLayout(BaseUI panel)
        {
            if (!Layouts.Contains(panel))
                Layouts.Add(panel);
        }

        IEnumerator TimerProcess(int time)
        {
            while (time > 0)
            {
                if (GameIsRunning)
                    statsPanel.SetTimer(--time);

                yield return new WaitForSeconds(1);
            }

            App.GameOver();
            GameIsRunning = false;
            string result = config.Score >= config.ScoresToWin ? "You win!" : "You lose!";
            resultPanel.SetPanel("RESULTS", result);
            ShowLayout<ResultPanel>();
        }

        public void SetScores(int score) => statsPanel.SetScore(score);

        public void NoMatch3Possibility()
        {
            if (!GameIsRunning) return;
            messagePanel.SetPanel("MESSAGE", "No match3 posibility found.\nLevel field has been reloaded.");
            ShowLayout<MessagePanel>();
        }

        public void OnPlayBtnClick()
        {
            ShowLayout<CreateLevelPanel>();
            statsPanel.Show(true);
        }

        public void OnCreateLevelBtnClick(int width, int height)
        {
            App.CreateLevelField(width, height);
            GameIsRunning = true;
            timerProcess = TimerProcess(30);
            StartCoroutine(timerProcess);
        }

        public void OnPauseBtnClick()
        {
            ShowLayout<PauseMenuPanel>();
        }

        public void OnRestartBtnClick()
        {
            StopCoroutine(timerProcess);
            ShowLayout<CreateLevelPanel>();
            statsPanel.ToDefault();
            App.Restart();
        }

        public void OnQuitBtnClick()
        {
            App.Quit();
        }

        [Serializable]
        class MessagePanel : BaseAnimPanel { }

    }
}