﻿using UnityEditor;
using UnityEngine;

namespace ASM.Project.Game.EditorUtil
{
    [CustomEditor(typeof(LevelManager))]
    public class CellInfoDrawer : Editor
    {
        void OnSceneGUI()
        {
            LevelManager mng = (LevelManager) target;
            if (mng == null) return;

            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.black;
            style.fontSize = 10;

            for (int x = 0; x < mng.items.GetLength(0); x++)
            {
                for (int y = 0; y < mng.items.GetLength(1); y++)
                {
                    if (mng.items[x, y] == null) continue;

                    var pos = mng.items[x, y].Tr.parent.TransformPoint(new Vector2(x, y));

                    var type_pos = new Vector2(pos.x, pos.y + .3f);
                    Handles.Label(type_pos, $"{(int) mng.items[x, y].Type}", style);

                    var cell_pos = new Vector2(pos.x - .3f, pos.y);
                    Handles.Label(cell_pos, $"[{x} : {y}]", style);
                }
            }
        }
    }
}